﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PCalProcessor
{
    /// <summary>
    /// Interaction logic for SondeDetails.xaml
    /// </summary>
    public partial class SondeDetails : Window
    {
        public analysisPressureCalibration.runData sonde { get; set; }  //Sonde data.

        public SondeDetails()
        {
            InitializeComponent();
        }

        private void details_Loaded(object sender, RoutedEventArgs e)
        {
            this.Title = sonde.SondeID + " " + this.Title;      //Updating the display name

            loadDataGrid();      //Loading grid with data.

        }

        void loadDataGrid()
        {
            DataGridTextColumn sp = new DataGridTextColumn();
            sp.Header = "Set Point";
            sp.Binding = new Binding("setPoint");
            sp.Width = 75;
            dataGridSPData.Columns.Add(sp);

            DataGridTextColumn refP = new DataGridTextColumn();
            refP.Header = "Ref P";
            refP.Binding = new Binding("refPressure");
            refP.Width = 75;
            dataGridSPData.Columns.Add(refP);

            DataGridTextColumn sondeP = new DataGridTextColumn();
            sondeP.Header = "Sonde P";
            sondeP.Binding = new Binding("sondePressure");
            sondeP.Width = 75;
            dataGridSPData.Columns.Add(sondeP);

            DataGridTextColumn pError = new DataGridTextColumn();
            pError.Header = "P Error";
            pError.Binding = new Binding("pError");
            pError.Width = 75;
            dataGridSPData.Columns.Add(pError);

            DataGridTextColumn stDevRefP = new DataGridTextColumn();
            stDevRefP.Header = "Ref StDev P";
            stDevRefP.Binding = new Binding("stDevRefP");
            stDevRefP.Width = 75;
            dataGridSPData.Columns.Add(stDevRefP);

            DataGridTextColumn stDevP = new DataGridTextColumn();
            stDevP.Header = "Sonde StDev P";
            stDevP.Binding = new Binding("stDevP");
            stDevP.Width = 75;
            dataGridSPData.Columns.Add(stDevP);

            DataGridTextColumn refAT = new DataGridTextColumn();
            refAT.Header = "Ref AT";
            refAT.Binding = new Binding("refAT");
            refAT.Width = 75;
            dataGridSPData.Columns.Add(refAT);

            DataGridTextColumn sondeAT = new DataGridTextColumn();
            sondeAT.Header = "Sonde AT";
            sondeAT.Binding = new Binding("sondeAT");
            sondeAT.Width = 75;
            dataGridSPData.Columns.Add(sondeAT);

            DataGridTextColumn ATError = new DataGridTextColumn();
            ATError.Header = "AT Error";
            ATError.Binding = new Binding("ATError");
            ATError.Width = 75;
            dataGridSPData.Columns.Add(ATError);

            DataGridTextColumn stDevRefAT = new DataGridTextColumn();
            stDevRefAT.Header = "Ref StDev AT";
            stDevRefAT.Binding = new Binding("stDevRefAT");
            stDevRefAT.Width = 75;
            dataGridSPData.Columns.Add(stDevRefAT);

            DataGridTextColumn stDevAT = new DataGridTextColumn();
            stDevAT.Header = "Sonde StDev AT";
            stDevAT.Binding = new Binding("stDevAT");
            stDevAT.Width = 75;
            dataGridSPData.Columns.Add(stDevAT);


            for(int i = 0; i< sonde.SetPoints.Count(); i++)
            {
                dataGridSPData.Items.Add(new dataLine(){setPoint = (i + 1).ToString(), refPressure = sonde.SetPoints[i].avgRefPressure.ToString("0.0000"),
                    sondePressure = sonde.SetPoints[i].avgSondePressure.ToString("0.0000"),
                    pError = sonde.SetPoints[i].avgPressureError.ToString("0.0000"),
                    refAT = sonde.SetPoints[i].avgRefAirTemp.ToString("0.0000"),
                    sondeAT = sonde.SetPoints[i].avgSondeAirTemp.ToString("0.0000"),
                    ATError = sonde.SetPoints[i].avgAirTempError.ToString("0.0000"),
                    stDevP = sonde.SetPoints[i].stDevSondePressure.ToString("0.0000"),
                    stDevAT = sonde.SetPoints[i].stDevSondeAirTemp.ToString("0.0000"),
                    stDevRefP = sonde.SetPoints[i].stDevRefPressure.ToString("0.0000"),
                    stDevRefAT = sonde.SetPoints[i].stDevRefAirTemp.ToString("0.0000")
                });
            }

            dataGridSPData.IsReadOnly = true;

        }

        /// <summary>
        /// Loads the curent set point data into the graph.
        /// </summary>
        void loadGraph()
        {
            //System.Windows.Controls.data
        }
    }

    public class dataLine
    {
        public string setPoint { get; set; }
        public string refPressure { get; set; }
        public string sondePressure { get; set; }
        public string pError { get; set; }
        public string refAT { get; set; }
        public string sondeAT { get; set; }
        public string ATError { get; set; }
        public string stDevP { get; set; }
        public string stDevAT { get; set; }
        public string stDevRefP { get; set; }
        public string stDevRefAT { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PCalProcessor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<groupFiles> curPCalFiles = new List<groupFiles>();


        public MainWindow()
        {
            InitializeComponent();
        }

        delegate void UpdateStatus(string message);

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void mnuItemSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void mnuLoad_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openGroup = new Microsoft.Win32.OpenFileDialog();
            openGroup.DefaultExt = "glog";
            openGroup.Multiselect = true;
            openGroup.Filter = "Group Files (.glog)|*.glog";

            if (openGroup.ShowDialog() == true)
            {
                gridDataDisplay.RowDefinitions.Clear();
                gridDataDisplay.ColumnDefinitions.Clear();
                gridDataDisplay.Children.Clear();
                curPCalFiles.Clear();
                mnuExport.IsEnabled = false;

                System.Threading.Thread loadData = new System.Threading.Thread(threadLoadData);
                loadData.Name = "Data Loader";
                loadData.Start(openGroup.FileNames);

            }
        }

        void threadLoadData(object e)
        {
            string[] logFiles = (string[])e;

            //Creating a compile list of the logged data and the referance file that is related to that data.
            foreach (string data in logFiles)
            {
                //Breaking down the current location of the log file to get the location.
                string[] dataBreakDown = data.Split('\\');

                groupFiles tempGroup = new groupFiles();

                string[] fileData = System.IO.File.ReadAllLines(data);

                for (int i = 0; i < fileData.Length - 1; i++)
                {
                    string[] lineBreak = fileData[i].Split('\\');
                    tempGroup.sondeFiles.Add(lineBreak[lineBreak.Length - 1]);
                }

                string[] refLineBreak = fileData[fileData.Length - 1].Split('\\');
                tempGroup.refFile = refLineBreak[refLineBreak.Length - 1];

                //finding the folder that the files are in.
                string[] filenameBreak = data.Split('\\');
                tempGroup.groupDir = data.Remove(data.Length - filenameBreak[filenameBreak.Length - 1].Length, filenameBreak[filenameBreak.Length - 1].Length);

                curPCalFiles.Add(tempGroup);

            }




            for (int i = 0; i < curPCalFiles.Count; i++)
            {
                analysisPressureCalibration.dataPressure fileCrunch = new analysisPressureCalibration.dataPressure();
                List<analysisPressureCalibration.refData> curRefData = fileCrunch.loadRefData(curPCalFiles[i].groupDir + curPCalFiles[i].refFile);

                for (int s = 0; s < curPCalFiles[i].sondeFiles.Count; s++)
                {

                    updateStatusBar("Processing: " + curPCalFiles[i].sondeFiles[s]);
                    
                    Pressure_Calculator.Pressure_Calculator rawProcessor = new Pressure_Calculator.Pressure_Calculator();
                    analysisPressureCalibration.runData tempProcessedData = new analysisPressureCalibration.runData();
                    tempProcessedData.SondeID = curPCalFiles[i].sondeFiles[s];
                    tempProcessedData.coeffiecients = rawProcessor.calculatePressureCoefficients(curPCalFiles[i].groupDir + curPCalFiles[i].sondeFiles[s],
                        curPCalFiles[i].groupDir + curPCalFiles[i].refFile);


                    List<analysisPressureCalibration.pressureData> curSondeData = fileCrunch.loadRawPData(curPCalFiles[i].groupDir + curPCalFiles[i].sondeFiles[s]);
                    List<analysisPressureCalibration.alignedData> curAlignedData = fileCrunch.alignListData(curSondeData, curRefData);
                    List<analysisPressureCalibration.spData> curSPData = fileCrunch.getSPData(curAlignedData);
                    curSPData = getCalcPandT(curSPData, tempProcessedData.coeffiecients);

                    curSPData = fileCrunch.getAvgStDev(curSPData);
                    tempProcessedData.SetPoints = curSPData;

                    List<double> errorPressure = new List<double>();
                    List<double> errorAirTemp = new List<double>();

                    foreach (analysisPressureCalibration.spData sp in tempProcessedData.SetPoints)
                    {
                        errorPressure.Add(sp.avgPressureError);
                        errorAirTemp.Add(sp.avgAirTempError);
                    }

                    tempProcessedData.avgPressureError = fileCrunch.calculateMean(errorPressure.ToArray());
                    tempProcessedData.avgAirTempError = fileCrunch.calculateMean(errorAirTemp.ToArray());

                    tempProcessedData.stDevErrorPressure = fileCrunch.calculateStDev(errorPressure.ToArray());
                    tempProcessedData.stDevErrorAirTemp = fileCrunch.calculateStDev(errorPressure.ToArray());

                    curPCalFiles[i].sondeProcessedData.Add(tempProcessedData);
                }

            }

            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => setupDataDisplay()));       //Setting up the header.

            int sondeCount = 1;

            for (int i = 0; i < curPCalFiles.Count; i++)
            {
                for (int p = 0; p < curPCalFiles[i].sondeProcessedData.Count; p++)
                {
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => addDataLine(curPCalFiles[i].sondeProcessedData[p], sondeCount, i)));
                    sondeCount++;
                }
            }

            updateStatusBar("All group files loaded");

            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => mnuExport.IsEnabled = true));
        }

        void setupDataDisplay()
        {
            //Header Row.
            gridDataDisplay.RowDefinitions.Add(new RowDefinition());        //Header row.
            gridDataDisplay.RowDefinitions[0].Height = new GridLength(30);  //Header row size.

            //Setting up the columns
            //Count
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Radiosonde Count
            gridDataDisplay.ColumnDefinitions[0].Width = new GridLength(50);   //Radiosonde Count Size.

            //Group
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Radiosonde Group
            gridDataDisplay.ColumnDefinitions[1].Width = new GridLength(50);   //Radiosonde Group Size.
            
            //SN
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Serial number info.
            gridDataDisplay.ColumnDefinitions[2].Width = new GridLength(100);   //Serial Number width.

            //Avg Pressure Error
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Serial number info.
            gridDataDisplay.ColumnDefinitions[3].Width = new GridLength(75);   //Serial Number width.

            //StDev Pressure Error
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Serial number info.
            gridDataDisplay.ColumnDefinitions[4].Width = new GridLength(75);   //Serial Number width.

            //Avg Air Error
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Serial number info.
            gridDataDisplay.ColumnDefinitions[5].Width = new GridLength(75);   //Serial Number width.
            
            //Avg StDev Error
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Serial number info.
            gridDataDisplay.ColumnDefinitions[6].Width = new GridLength(75);   //Serial Number width.

            //Detail Button
            gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());  //Detail Button
            gridDataDisplay.ColumnDefinitions[7].Width = new GridLength(150);   //Detail Button width.


            //Filling in the columns
            //Count
            Label labelCount = new Label();
            labelCount.Content = "Count";
            labelCount.SetValue(Grid.RowProperty, 0);
            labelCount.SetValue(Grid.ColumnProperty, 0);
            labelCount.BorderBrush = Brushes.Black;
            labelCount.BorderThickness = new Thickness(1);
            labelCount.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelCount.Background = Brushes.LightGray;

            //Group
            Label labelGroup = new Label();
            labelGroup.Content = "Group";
            labelGroup.SetValue(Grid.RowProperty, 0);
            labelGroup.SetValue(Grid.ColumnProperty, 1);
            labelGroup.BorderBrush = Brushes.Black;
            labelGroup.BorderThickness = new Thickness(1);
            labelGroup.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelGroup.Background = Brushes.LightGray;

            //Serial Number
            Label labelSerialNumber = new Label();
            labelSerialNumber.Content = "Sonde SN";
            labelSerialNumber.SetValue(Grid.RowProperty, 0);
            labelSerialNumber.SetValue(Grid.ColumnProperty, 2);
            labelSerialNumber.BorderBrush = Brushes.Black;
            labelSerialNumber.BorderThickness = new Thickness(1);
            labelSerialNumber.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelSerialNumber.Background = Brushes.LightGray;

            //Avg Pressure Error
            Label labelPError = new Label();
            labelPError.Content = "P Err";
            labelPError.SetValue(Grid.RowProperty, 0);
            labelPError.SetValue(Grid.ColumnProperty, 3);
            labelPError.BorderBrush = Brushes.Black;
            labelPError.BorderThickness = new Thickness(1);
            labelPError.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelPError.Background = Brushes.LightGray;

            //StDev Pressure Error
            Label labelStDevP = new Label();
            labelStDevP.Content = "StDev P";
            labelStDevP.SetValue(Grid.RowProperty, 0);
            labelStDevP.SetValue(Grid.ColumnProperty, 4);
            labelStDevP.BorderBrush = Brushes.Black;
            labelStDevP.BorderThickness = new Thickness(1);
            labelStDevP.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelStDevP.Background = Brushes.LightGray;

            //Avg Air Temp Error
            Label labelATError = new Label();
            labelATError.Content = "AT Err";
            labelATError.SetValue(Grid.RowProperty, 0);
            labelATError.SetValue(Grid.ColumnProperty, 5);
            labelATError.BorderBrush = Brushes.Black;
            labelATError.BorderThickness = new Thickness(1);
            labelATError.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelATError.Background = Brushes.LightGray;

            //StDev Air Temp Error
            Label labelStDevAT = new Label();
            labelStDevAT.Content = "StDev AT";
            labelStDevAT.SetValue(Grid.RowProperty, 0);
            labelStDevAT.SetValue(Grid.ColumnProperty, 6);
            labelStDevAT.BorderBrush = Brushes.Black;
            labelStDevAT.BorderThickness = new Thickness(1);
            labelStDevAT.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelStDevAT.Background = Brushes.LightGray;

            //StDev Air Temp Error
            Label labelDetails = new Label();
            labelDetails.Content = "Details";
            labelDetails.SetValue(Grid.RowProperty, 0);
            labelDetails.SetValue(Grid.ColumnProperty, 7);
            labelDetails.BorderBrush = Brushes.Black;
            labelDetails.BorderThickness = new Thickness(1);
            labelDetails.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelDetails.Background = Brushes.LightGray;



            //Adding element to the grid display.
            gridDataDisplay.Children.Add(labelCount);
            gridDataDisplay.Children.Add(labelGroup);
            gridDataDisplay.Children.Add(labelSerialNumber);
            gridDataDisplay.Children.Add(labelPError);
            gridDataDisplay.Children.Add(labelStDevP);
            gridDataDisplay.Children.Add(labelATError);
            gridDataDisplay.Children.Add(labelStDevAT);
            //gridDataDisplay.Children.Add(labelDetails);

            

        }

        void addDataLine(analysisPressureCalibration.runData sondeRunData, int count, int group)
        {
            gridDataDisplay.RowDefinitions.Add(new RowDefinition());        
            gridDataDisplay.RowDefinitions.Last().Height = new GridLength(30);

            //Filling in the columns
            //Count
            Label labelCount = new Label();
            labelCount.Content = count.ToString();
            labelCount.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelCount.SetValue(Grid.ColumnProperty, 0);
            labelCount.BorderBrush = Brushes.Black;
            labelCount.BorderThickness = new Thickness(1);
            labelCount.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelCount.Background = Brushes.White;

            //Group
            Label labelGroup = new Label();
            labelGroup.Content = group.ToString();
            labelGroup.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelGroup.SetValue(Grid.ColumnProperty, 1);
            labelGroup.BorderBrush = Brushes.Black;
            labelGroup.BorderThickness = new Thickness(1);
            labelGroup.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelGroup.Background = Brushes.White;

            //Serial Number
            Label labelSerialNumber = new Label();
            labelSerialNumber.Content = sondeRunData.SondeID;
            labelSerialNumber.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelSerialNumber.SetValue(Grid.ColumnProperty, 2);
            labelSerialNumber.BorderBrush = Brushes.Black;
            labelSerialNumber.BorderThickness = new Thickness(1);
            labelSerialNumber.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelSerialNumber.Background = Brushes.White;

            //Avg Pressure Error
            Label labelPError = new Label();
            labelPError.Content = sondeRunData.avgPressureError.ToString("0.0000");
            labelPError.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelPError.SetValue(Grid.ColumnProperty, 3);
            labelPError.BorderBrush = Brushes.Black;
            labelPError.BorderThickness = new Thickness(1);
            labelPError.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelPError.Background = Brushes.White;

            //StDev Pressure Error
            Label labelStDevP = new Label();
            labelStDevP.Content = sondeRunData.stDevErrorPressure.ToString("0.0000");
            labelStDevP.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelStDevP.SetValue(Grid.ColumnProperty, 4);
            labelStDevP.BorderBrush = Brushes.Black;
            labelStDevP.BorderThickness = new Thickness(1);
            labelStDevP.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelStDevP.Background = Brushes.White;

            //Avg Air Temp Error
            Label labelATError = new Label();
            labelATError.Content = sondeRunData.avgAirTempError.ToString("0.000");
            labelATError.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelATError.SetValue(Grid.ColumnProperty, 5);
            labelATError.BorderBrush = Brushes.Black;
            labelATError.BorderThickness = new Thickness(1);
            labelATError.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelATError.Background = Brushes.White;

            //StDev Air Temp Error
            Label labelStDevAT = new Label();
            labelStDevAT.Content = sondeRunData.stDevErrorAirTemp.ToString("0.0000");
            labelStDevAT.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            labelStDevAT.SetValue(Grid.ColumnProperty, 6);
            labelStDevAT.BorderBrush = Brushes.Black;
            labelStDevAT.BorderThickness = new Thickness(1);
            labelStDevAT.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            labelStDevAT.Background = Brushes.White;

            Button buttonDetails = new Button();
            buttonDetails.Content = sondeRunData.SondeID + " Details";
            buttonDetails.Width = gridDataDisplay.ColumnDefinitions.Last().Width.Value - 5;
            buttonDetails.Height = gridDataDisplay.RowDefinitions[gridDataDisplay.RowDefinitions.Count - 1].Height.Value - 5;
            buttonDetails.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            buttonDetails.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
            buttonDetails.SetValue(Grid.RowProperty, gridDataDisplay.RowDefinitions.Count - 1);
            buttonDetails.SetValue(Grid.ColumnProperty, 7);
            buttonDetails.Click += new RoutedEventHandler(buttonDetails_Click);

            //Adding element to the grid display.
            gridDataDisplay.Children.Add(labelCount);
            gridDataDisplay.Children.Add(labelGroup);
            gridDataDisplay.Children.Add(labelSerialNumber);
            gridDataDisplay.Children.Add(labelPError);
            gridDataDisplay.Children.Add(labelStDevP);
            gridDataDisplay.Children.Add(labelATError);
            gridDataDisplay.Children.Add(labelStDevAT);
            gridDataDisplay.Children.Add(buttonDetails);


            //Adjusting gride view
            gridDataDisplay.Height = (gridDataDisplay.RowDefinitions.Count * (int)gridDataDisplay.RowDefinitions.Last().Height.Value)+20;

        }

        void buttonDetails_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("test");

            Button incomingButton = (Button)sender;
            string[] buttonName = incomingButton.Content.ToString().Split(' ');

            for (int i = 0; i < curPCalFiles.Count; i++)
            {
                for(int p = 0; p< curPCalFiles[i].sondeProcessedData.Count; p++)
                {
                    if (curPCalFiles[i].sondeProcessedData[p].SondeID == buttonName[0])
                    {
                        SondeDetails details = new SondeDetails();
                        details.sonde = curPCalFiles[i].sondeProcessedData[p];
                        details.Show();
                    }
                }
            }


        }

        /*
                void addDataLine()
                {
            
                    gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());
                    //gridDataDisplay.ColumnDefinitions[0].Width = System.Windows.GridLength.Auto;

                    gridDataDisplay.ColumnDefinitions.Add(new ColumnDefinition());
                    //gridDataDisplay.ColumnDefinitions[1].Width = System.Windows.GridLength.Auto;

                    gridDataDisplay.RowDefinitions.Add(new RowDefinition());
                    gridDataDisplay.RowDefinitions[0].Height = new GridLength(200);
                    gridDataDisplay.RowDefinitions.Add(new RowDefinition());
                    gridDataDisplay.RowDefinitions[1].Height = new GridLength(200);
                    gridDataDisplay.RowDefinitions.Add(new RowDefinition());
                    gridDataDisplay.RowDefinitions[2].Height = new GridLength(200);

                    gridDataDisplay.Height = gridDataDisplay.RowDefinitions.Count * 200;

                    Button btn;
                    int i = 0;
                    Random randomColor = new Random();

                    for (int row = 0; row < gridDataDisplay.RowDefinitions.Count; row++)
                    {
                        for (int col = 0; col < gridDataDisplay.ColumnDefinitions.Count; col++)
                        {
                            i++;
                            /*
                            btn = new Button();
                            btn.Width = 200;
                            btn.Height = 200;
                            btn.Content = "test";

                            BrushConverter converter = new BrushConverter();
                            //Brush brush = new Brush(); //(Brush)converter.ConvertFrom(Color.FromRgb((byte)randomColor.Next(0, 255), (byte)randomColor.Next(0, 255), (byte)randomColor.Next(0, 255)));
                            btn.Background = new SolidColorBrush(Color.FromRgb((byte)randomColor.Next(0, 255), (byte)randomColor.Next(0, 255), (byte)randomColor.Next(0, 255)));

                            btn.FontSize = 20;
                            btn.Margin = new Thickness(1);
                            btn.Padding = new Thickness(100);
                            btn.VerticalAlignment = VerticalAlignment.Center;
                            btn.SetValue(Grid.ColumnProperty, col);
                            btn.SetValue(Grid.RowProperty, row);
                            gridDataDisplay.Children.Add(btn);
                             */
        /*          
                          Label curLabel = new Label();
                          curLabel.Width = 200;
                          curLabel.Height = 200;

                          //curLabel.Margin = new Thickness(1);
                          //curLabel.Padding = new Thickness(20);

                          curLabel.FontSize = 20;

                          curLabel.BorderBrush = Brushes.Black;
                          curLabel.BorderThickness = new Thickness(1);
                          curLabel.Content = i.ToString();
                          curLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                          curLabel.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
                          curLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                          curLabel.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
                          curLabel.SetValue(Grid.ColumnProperty, col);
                          curLabel.SetValue(Grid.RowProperty, row);
                          gridDataDisplay.Children.Add(curLabel);
                      }
                  }
            
              }
              */

        List<analysisPressureCalibration.spData> getCalcPandT(List<analysisPressureCalibration.spData> spData, double[] coef)
        {
            Pressure_Calculator.Pressure_Calculator processor = new Pressure_Calculator.Pressure_Calculator();

            for (int i = 0; i < spData.Count; i++)
            {
                for (int x = 0; x < spData[i].rawSPData.Count; x++)
                {
                    spData[i].rawSPData[x].sData.calcAirTemp = processor.calcPressureTemp(spData[i].rawSPData[x].sData.ptCount, spData[i].rawSPData[x].sData.ptCountRef);
                    spData[i].rawSPData[x].sData.calcPressure = processor.calcPressure(spData[i].rawSPData[x].sData.pressureCount, spData[i].rawSPData[x].sData.pressureCountRef, spData[i].rawSPData[x].sData.calcAirTemp, coef);
                }
            }

            return spData;
        }

        #region Methods for updating the screen.

        void updateStatusBar(string message)
        {
            Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => this.labelStatusMain.Content = message));
        }

        #endregion

        private void mnuItemSingleCSV_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog saveLoc = new System.Windows.Forms.FolderBrowserDialog();

            if (System.Windows.Forms.DialogResult.OK == saveLoc.ShowDialog())
            {

                for (int i = 0; i < curPCalFiles.Count; i++)
                {
                    for (int sonde = 0; sonde < curPCalFiles[i].sondeProcessedData.Count; sonde++)
                    {
                        analysisPressureCalibration.dataPressure saver = new analysisPressureCalibration.dataPressure();

                        string[] nameBreak = curPCalFiles[i].sondeProcessedData[sonde].SondeID.Split(new char[] { '_', '.' });

                        updateStatusBar("Writing: " + saveLoc.SelectedPath + "\\" + nameBreak[1] + ".csv");

                        saver.saveProcessedData(saveLoc.SelectedPath + "\\" + nameBreak[1] + ".csv", curPCalFiles[i].sondeProcessedData[sonde]);

                    }
                }
            }
        }

        private void mnuCompare_Click(object sender, RoutedEventArgs e)
        {
            compareWindow windowCompare = new compareWindow();
            windowCompare.calData = curPCalFiles;
            windowCompare.Show();
        }
    }

    public class groupFiles
    {
        public List<string> sondeFiles = new List<string>();
        public string refFile;
        public string groupDir;
        public List<analysisPressureCalibration.runData> sondeProcessedData = new List<analysisPressureCalibration.runData>();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization.Charting;

namespace PCalProcessor
{
    /// <summary>
    /// Interaction logic for compareWindow.xaml
    /// </summary>
    public partial class compareWindow : Window
    {
        public List<groupFiles> calData { get; set; }

        public List<Grid> listGridChart = new List<Grid>();

        public List<Chart> listCharts = new List<Chart>();

        public compareWindow()
        {
            InitializeComponent();
        }

        private void windowCompare_Loaded(object sender, RoutedEventArgs e)
        {
            setupRadiosondeIDs();    //Making the radiosonde ids appear.

            setupTabs();    //Setting up the display tabs.

            //loadAllSondeData(); //Starting with all data presented.
        }

        void setupRadiosondeIDs()
        {
            for(int i = 0; i< calData.Count; i++)
            {
                for(int sonde = 0; sonde< calData[i].sondeProcessedData.Count; sonde++)
                {
                    //Setting up rows to display the sonde.
                    RowDefinition tempRow = new RowDefinition();
                    tempRow.Name = "rowSonde";
                    tempRow.Height = new GridLength(23);
                    gridRadiosondeID.RowDefinitions.Add(tempRow);

                    //Setting up the check box and into the row.
                    CheckBox tempSondeCheck = new CheckBox();
                    tempSondeCheck.Name = "checkBoxSondeID";
                    tempSondeCheck.Content = calData[i].sondeProcessedData[sonde].SondeID;
                    tempSondeCheck.SetValue(Grid.RowProperty, gridRadiosondeID.RowDefinitions.Count - 1);
                    tempSondeCheck.SetValue(Grid.ColumnProperty, 0);
                    //tempSondeCheck.IsChecked = true;
                    tempSondeCheck.Checked += TempSondeCheck_Checked;
                    tempSondeCheck.Unchecked += TempSondeCheck_Unchecked;

                    gridRadiosondeID.Children.Add(tempSondeCheck);
                }
            }

            gridRadiosondeID.Height = (gridRadiosondeID.RowDefinitions.Count * 23) + 10;

        }

        void setupTabs()
        {
            TabItem stDevRefPressure = new TabItem();
            TabItem stDevSondePressure = new TabItem();
            TabItem stDevRefTemp = new TabItem();
            TabItem stDevSondeTemp = new TabItem();
            TabItem errorPressure = new TabItem();
            TabItem errorAirTemp = new TabItem();

            //Defining the tabs

            //StDev Ref Pressure
            stDevRefPressure.Name = "tabStDevRefPressure";
            stDevRefPressure.Header = "StDev Ref Pressure";
            stDevRefPressure.Content = addGridToTab("gridStDevRefPressure");
            addChartToGrid("gridStDevRefPressure", "chartStDevRefPressure");

            stDevSondePressure.Name = "tabStDevSondePressure";
            stDevSondePressure.Header = "StDev Sonde Pressure";
            stDevSondePressure.Content = addGridToTab("gridStDevSondePressure");
            addChartToGrid("gridStDevSondePressure", "chartStDevSondePressure");

            stDevRefTemp.Name = "tabStDevRefTemp";
            stDevRefTemp.Header = "StDev Ref Air Temp";
            stDevRefTemp.Content = addGridToTab("gridStDevRefTemp");
            addChartToGrid("gridStDevRefTemp", "chartStDevRefTemp");

            stDevSondeTemp.Name = "tabStDevSondeTemp";
            stDevSondeTemp.Header = "StDev Sonde Temp";
            stDevSondeTemp.Content = addGridToTab("gridStDevSondeTemp");
            addChartToGrid("gridStDevSondeTemp", "chartStDevSondeTemp");

            errorPressure.Name = "tabErrorPressure";
            errorPressure.Header = "Pressure Error";
            errorPressure.Content = addGridToTab("gridErrorPressure");
            addChartToGrid("gridErrorPressure", "chartErrorPressure");

            errorAirTemp.Name = "tabErrorTemp";
            errorAirTemp.Header = "Temperature Error";
            errorAirTemp.Content = addGridToTab("gridErrorTemp");
            addChartToGrid("gridErrorTemp", "chartErrorTemp");

            //Adding the tabs to the tab control
            tabControlDisplay.Items.Insert(0, stDevSondePressure);
            tabControlDisplay.Items.Insert(1, stDevSondeTemp);
            tabControlDisplay.Items.Insert(2, stDevRefPressure);
            tabControlDisplay.Items.Insert(3, stDevRefTemp);
            tabControlDisplay.Items.Insert(4, errorPressure);
            tabControlDisplay.Items.Insert(5, errorAirTemp);


            //Setting the starting location.
            tabControlDisplay.SelectedIndex = 0;
        }

        private void TempSondeCheck_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox incomingItem = (CheckBox)sender;

            if(incomingItem.IsChecked == true)
            {
                for (int g = 0; g < calData.Count; g++)
                {
                    for (int sonde = 0; sonde < calData[g].sondeProcessedData.Count; sonde++)
                    {
                        if (calData[g].sondeProcessedData[sonde].SondeID == (string)incomingItem.Content)
                        { 
                            List<Point> stDevRefP = new List<Point>();
                            List<Point> stDevSondeP = new List<Point>();
                            List<Point> stDevRefTemp = new List<Point>();
                            List<Point> stDevSondeTemp = new List<Point>();
                            List<Point> errorPressure = new List<Point>();
                            List<Point> errorTemp = new List<Point>();
                            

                            for (int sp = 0; sp < calData[g].sondeProcessedData[sonde].SetPoints.Count; sp++)
                            {
                                stDevRefP.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].stDevRefPressure));
                                stDevSondeP.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].stDevSondePressure));
                                stDevRefTemp.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].stDevRefAirTemp));
                                stDevSondeTemp.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].stDevSondeAirTemp));
                                errorPressure.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].avgRefPressure - calData[g].sondeProcessedData[sonde].SetPoints[sp].avgSondePressure));
                                errorTemp.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].avgRefAirTemp - calData[g].sondeProcessedData[sonde].SetPoints[sp].avgSondeAirTemp));
                            }

                            addSeries("chartStDevRefPressure", calData[g].sondeProcessedData[sonde].SondeID, stDevRefP);
                            addSeries("chartStDevSondePressure", calData[g].sondeProcessedData[sonde].SondeID, stDevSondeP);
                            addSeries("chartStDevRefTemp", calData[g].sondeProcessedData[sonde].SondeID, stDevRefTemp);
                            addSeries("chartStDevSondeTemp", calData[g].sondeProcessedData[sonde].SondeID, stDevSondeTemp);
                            addSeries("chartErrorPressure", calData[g].sondeProcessedData[sonde].SondeID, errorPressure);
                            addSeries("chartErrorTemp", calData[g].sondeProcessedData[sonde].SondeID, errorTemp);
                        }
                    }
                }
            }


        }

        private void TempSondeCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox incomingCheckBox = (CheckBox)sender;

            for (int i = 0; i < listCharts.Count; i++)
            {
                for (int s = 0; s < listCharts[i].Series.Count; s++)
                {
                    LineSeries cur = (LineSeries)listCharts[i].Series[s];

                    if (cur.Title == incomingCheckBox.Content)
                    {
                        listCharts[i].Series.RemoveAt(s);
                    }
                }
            }
            
        }

        Grid addGridToTab(string gridName)
        {
            Grid tabGrid = new Grid();
            tabGrid.Name = gridName;
            tabGrid.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#7F8CBF"));
            tabGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            tabGrid.VerticalAlignment = VerticalAlignment.Stretch;
            listGridChart.Add(tabGrid);

            return tabGrid;
        }

        void addChartToGrid(string gridName, string chartName)
        {
            Chart dataChart = new Chart();
            dataChart.Name = chartName;
            dataChart.HorizontalAlignment = HorizontalAlignment.Stretch;
            dataChart.VerticalAlignment = VerticalAlignment.Stretch;
            dataChart.SetValue(Grid.ColumnProperty, 0);
            dataChart.SetValue(Grid.RowProperty, 0);

            //Locating the grid to add the chart to.
            for (int i = 0; i < listGridChart.Count; i++)
            {
                if (listGridChart[i].Name == gridName)
                {
                    listCharts.Add(dataChart);
                    listGridChart[i].Children.Add(dataChart);
                }
            }
        }

        void loadAllSondeData()
        {
            for (int g = 0; g < calData.Count; g++)
            {
                for(int sonde = 0; sonde< calData[g].sondeProcessedData.Count; sonde++)
                {
                    List<Point> stDevRefP = new List<Point>();
                    for(int sp = 0; sp< calData[g].sondeProcessedData[sonde].SetPoints.Count; sp++)
                    {
                        stDevRefP.Add(new Point(sp, calData[g].sondeProcessedData[sonde].SetPoints[sp].stDevRefPressure));
                    }

                    addSeries("chartStDevRefPressure", calData[g].sondeProcessedData[sonde].SondeID, stDevRefP);
                }
            }
        }

        void addSeries(string chartName, string seriesName, List<Point>dataToChart)
        {

            LineSeries tempSeries = new LineSeries();
            //tempSeries.Name = seriesName;
            tempSeries.Title = seriesName;
            tempSeries.DependentValuePath = "Y";
            tempSeries.IndependentValuePath = "X";
            tempSeries.ItemsSource = dataToChart;

            for (int i = 0; i < listCharts.Count; i++)
            {
                if (listCharts[i].Name == chartName)
                {
                    listCharts[i].Series.Add(tempSeries);
                }
            }

        }

        void removeSeries(string chartName, string seriesName)
        {

        }

        private void mnuSaveUnchecked_Click(object sender, RoutedEventArgs e)
        {
            List<string> saveIDs = new List<string>();

            for (int i = 0; i < gridRadiosondeID.Children.Count; i++)
            {
                if (gridRadiosondeID.Children[i].GetType().ToString() == "System.Windows.Controls.CheckBox")
                {
                    CheckBox toTest = (CheckBox)gridRadiosondeID.Children[i];
                    if (toTest.IsChecked == false)
                    {
                        saveIDs.Add((string)toTest.Content);
                    }
                }
            }




            System.Windows.Forms.FolderBrowserDialog saveLoc = new System.Windows.Forms.FolderBrowserDialog();

            if (System.Windows.Forms.DialogResult.OK == saveLoc.ShowDialog())
            {

                //Writing some condtions.
                string[] currentTab = tabControlDisplay.Items[tabControlDisplay.SelectedIndex].ToString().Split(':');

                System.IO.File.WriteAllLines(saveLoc.SelectedPath + "\\" + currentTab[1] + ".txt", saveIDs.ToArray());

                for (int l = 0; l < saveIDs.Count; l++)
                {
                    for (int i = 0; i < calData.Count; i++)
                    {
                        for (int sonde = 0; sonde < calData[i].sondeProcessedData.Count; sonde++)
                        {
                            if (calData[i].sondeProcessedData[sonde].SondeID == saveIDs[l])
                            {
                                analysisPressureCalibration.dataPressure saver = new analysisPressureCalibration.dataPressure();

                                string[] nameBreak = calData[i].sondeProcessedData[sonde].SondeID.Split(new char[] { '_', '.' });

                                saver.saveProcessedData(saveLoc.SelectedPath + "\\" + nameBreak[1] + ".csv", calData[i].sondeProcessedData[sonde]);
                            }

                        }
                    }
                }
            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace analysisPressureCalibration
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            #region Test Code
            /*
            dataPressure pData = new dataPressure();
            pData.loadRawSondeData(@"\\sifalcon\users\Wjones\Logged Data\20140903-1551\_27022.p_raw");
            pData.loadReferanceData(@"\\sifalcon\users\Wjones\Logged Data\20140903-1551\1_Paroscientific_785.p_ref");
            
            List<List<rawPDataPoint>> fileSPData = pData.correlateData();

            List<rawPDataPoint> avgData = new List<rawPDataPoint>();

            foreach (List<rawPDataPoint> sp in fileSPData)
            {
                rawPDataPoint avgTest = pData.avgSPData(sp);
                avgData.Add(avgTest);
            }

            */

            #endregion  //End of test code.


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}

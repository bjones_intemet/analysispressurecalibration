﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace analysisPressureCalibration
{
    //Data and Process for raw data.
    class dataPressure
    {
        public List<rawPDataPoint> curData = new List<rawPDataPoint>();

        public dataPressure() { }

        /// <summary>
        /// Method loads raw radioosonde calibration data.
        /// </summary>
        /// <param name="filename"></param>
        public void loadRawSondeData(string filename)
        {
            //loading in the whole file.
            if (System.IO.File.Exists(filename))    //Checking to make sure the file is there.
            {
                string[] allData = System.IO.File.ReadAllLines(filename);

                //Checking for the right type of file.
                if(!allData[0].Contains("Pressure Calibration Process -- Raw Radiosonde Data"))
                {
                    throw new Exception("Wrong file format.");
                }

                //Getting the file/sonde ID
                string[] filenameBreak = filename.Split('\\');

                //Processing the file in to raw data objects.
                for (int i = 5; i < allData.Length; i++)
                {
                    string[] lineBreak = allData[i].Split(',');

                    rawPDataPoint tempData = new rawPDataPoint();
                    tempData.timeStamp = Convert.ToDouble(lineBreak[0]);
                    tempData.pressureCount = Convert.ToInt32(lineBreak[1]);
                    tempData.pressureCountRef = Convert.ToInt32(lineBreak[2]);
                    tempData.ptCount = Convert.ToInt32(lineBreak[3]);
                    tempData.ptCountRef = Convert.ToInt32(lineBreak[4]);

                    tempData.sondeID = filenameBreak[filenameBreak.Length - 1].Remove(0, 1);
                    tempData.sondeID = tempData.sondeID.Remove(tempData.sondeID.Length - 6, 6);

                    curData.Add(tempData);
                }

            }
            else
            {
                throw new Exception("No File Found.");
            }
        }

        public void loadReferanceData(string filename)
        {
            //loading in the whole file.
            if (System.IO.File.Exists(filename))    //Checking to make sure the file is there.
            {
                string[] allData = System.IO.File.ReadAllLines(filename);

                //Processing the file in to raw data objects.
                for (int i = 5; i < allData.Length; i++)
                {
                    string[] lineBreak = allData[i].Split(',');

                    foreach (rawPDataPoint raw in curData)
                    {
                        if (Convert.ToDouble(lineBreak[0]) == raw.timeStamp)
                        {
                            raw.refPressure = Convert.ToDouble(lineBreak[1]);
                            raw.refAirTemp = Convert.ToDouble(lineBreak[2]);
                        }
                    }
                }

            }
            else
            {
                throw new Exception("No File Found.");
            }
        }

        public List<pressureData> loadRawPData(string fileName)
        {
            List<pressureData> rawSondeData = new List<pressureData>();     //All loaded pressure calibration data.

            string[] fileData = System.IO.File.ReadAllLines(fileName);      //Loading in the file data.

            for (int j = 5; j < fileData.Length; j++)   //Processing the raw pressure data
            {

                string[] fileLineBreak = fileData[j].Split(',');    //Breaking down the line into element to process.

                pressureData tempData = new pressureData();     //Contianer for the fomatted data.

                tempData.time = Convert.ToDouble(fileLineBreak[0]);     //Time data collected.
                tempData.pressureCount = Convert.ToInt32(fileLineBreak[1]);     //Pressure Count.
                tempData.pressureCountRef = Convert.ToInt32(fileLineBreak[2]);  //Ramp Reference Count.
                tempData.ptCount = Convert.ToInt32(fileLineBreak[3]);           //Pressure Temperature Count.
                tempData.ptCountRef = Convert.ToInt32(fileLineBreak[4]);        //Pressure Temperearure Ramp Reference

                rawSondeData.Add(tempData);
            }

            return rawSondeData;
        }

        public List<refData> loadRefData(string fileName)
        {
            string[] refFileData = System.IO.File.ReadAllLines(fileName);   //Loading the reference data.

            List<refData> pRefData = new List<refData>();   //Continer for processed reference data.

            for (int j = 5; j < refFileData.Length; j++)    //Process loop.
            {
                string[] refFileBreak = refFileData[j].Split(',');  //Breaking down the reference line.

                refData tempRef = new refData();    //Temp container for the reference line processed data.

                tempRef.time = Convert.ToDouble(refFileBreak[0]);       //Reference Time.
                tempRef.pressure = Convert.ToDouble(refFileBreak[1]);       //Reference Pressure.
                tempRef.temperature = Convert.ToDouble(refFileBreak[2]);    //Reference Air Temp.

                pRefData.Add(tempRef);  //Adding to the list arry
            }

            return pRefData;
        }

        public List<alignedData> alignListData(List<pressureData> rawSondeData, List<refData> pRefData)
        {
            List<alignedData> finalData = new List<alignedData>();

            for (int i = 0; i < pRefData.Count; i++)
            {
                for (int x = 0; x < rawSondeData.Count; x++)
                {
                    if (pRefData[i].time == rawSondeData[x].time)
                    {
                        alignedData tempAligned = new alignedData();
                        tempAligned.pRef = pRefData[i];
                        tempAligned.sData = rawSondeData[x];

                        finalData.Add(tempAligned);
                    }

                }
            }

            return finalData;
        }

        public List<spData> getSPData(List<alignedData> data)
        {
            List<spData> theSetPoints = new List<spData>();

            spData tempSP = new spData();
            tempSP.rawSPData.Add(data[0]);

            for (int i = 1; i < data.Count; i++)
            {
                if (data[i].pRef.pressure > tempSP.rawSPData.First().pRef.pressure - 1 && data[i].pRef.pressure < tempSP.rawSPData.First().pRef.pressure + 1 &&
                   data[i].pRef.temperature > tempSP.rawSPData.First().pRef.temperature - 1 && data[i].pRef.temperature < tempSP.rawSPData.First().pRef.temperature + 1)
                {
                    tempSP.rawSPData.Add(data[i]);
                }
                else
                {
                    theSetPoints.Add(tempSP);
                    tempSP = new spData();
                    tempSP.rawSPData.Add(data[i]);
                }
            }

            if (tempSP.rawSPData.Count > 0)
            {
                theSetPoints.Add(tempSP);
            }


            return theSetPoints;
        }

        public List<spData> getAvgStDev(List<spData> data)
        {
            

            for (int i = 0; i < data.Count; i++)
            {
                List<double> refPressure = new List<double>();
                List<double> refAirTemp = new List<double>();

                List<double> sondePCount = new List<double>();
                List<double> sondePRCount = new List<double>();
                List<double> sondeTCount = new List<double>();
                List<double> sondeTRCount = new List<double>();

                for (int d = 0; d < data[i].rawSPData.Count; d++)
                {
                    refPressure.Add(data[i].rawSPData[d].pRef.pressure);
                    refAirTemp.Add(data[i].rawSPData[d].pRef.temperature);

                    sondePCount.Add(data[i].rawSPData[d].sData.pressureCount);
                    sondePRCount.Add(data[i].rawSPData[d].sData.pressureCountRef);
                    sondeTCount.Add(data[i].rawSPData[d].sData.ptCount);
                    sondeTRCount.Add(data[i].rawSPData[d].sData.ptCountRef);
                }

                data[i].avgRefAirTemp = calculateMean(refAirTemp.ToArray());
                data[i].avgRefPressure = calculateMean(refPressure.ToArray());

                data[i].avgSondePCount = calculateMean(sondePCount.ToArray());
                data[i].avgSondePRCount = calculateMean(sondePRCount.ToArray());
                data[i].avgSondeTCount = calculateMean(sondeTCount.ToArray());
                data[i].avgSondeTRCount = calculateMean(sondeTRCount.ToArray());

                data[i].stDevRefAirTemp = calculateStDev(refAirTemp.ToArray());
                data[i].stDevRefPressure = calculateStDev(refPressure.ToArray());

                data[i].stDevSondePCount = calculateStDev(sondePCount.ToArray());
                data[i].stDevSondePRCount = calculateStDev(sondePRCount.ToArray());
                data[i].stDevSondeTCount = calculateStDev(sondeTCount.ToArray());
                data[i].stDevSondeTRCount = calculateStDev(sondeTRCount.ToArray());
            }

            return data;
        }


        #region Methods used in processing that data.

        public List<List<rawPDataPoint>> correlateData()
        {
            List<List<rawPDataPoint>> corDataPoint = new List<List<rawPDataPoint>>();   //Groupings of the test points raw data.

            if (curData.Count > 0)
            {
                //Starting point for data search
                int startPoint = 0;

                List<rawPDataPoint> tempCollection = new List<rawPDataPoint>();

                for (int i = 0; i < curData.Count; i++)
                {
                    if (curData[i].timeStamp >= curData[startPoint].timeStamp && curData[i].timeStamp < curData[startPoint].timeStamp + 300)
                    {
                        tempCollection.Add(curData[i]);
                    }
                    else
                    {
                        corDataPoint.Add(tempCollection);
                        startPoint = i;
                        tempCollection = new List<rawPDataPoint>();
                    }
                }

                corDataPoint.Add(tempCollection);

                return corDataPoint;

            }

            return null;
        }

        public rawPDataPoint avgSPData(List<rawPDataPoint> spData)
        {
            if (spData != null && spData.Count > 0)
            {
                rawPDataPoint avgPointOutput = new rawPDataPoint();

                for (int i = 0; i < spData.Count; i++)
                {
                    //Adding everything up.
                    avgPointOutput.timeStamp += 1;
                    avgPointOutput.pressureCount += spData[i].pressureCount;
                    avgPointOutput.pressureCountRef += spData[i].pressureCountRef;
                    avgPointOutput.ptCount += spData[i].ptCount;
                    avgPointOutput.ptCountRef += spData[i].ptCountRef;
                    avgPointOutput.refPressure += spData[i].refPressure;
                    avgPointOutput.refAirTemp += spData[i].refAirTemp;
                }

                //Getting the average.
                avgPointOutput.pressureCount = avgPointOutput.pressureCount / spData.Count;
                avgPointOutput.pressureCountRef = avgPointOutput.pressureCountRef / spData.Count;
                avgPointOutput.ptCount = avgPointOutput.ptCount / spData.Count;
                avgPointOutput.ptCountRef = avgPointOutput.ptCountRef / spData.Count;
                avgPointOutput.refPressure = avgPointOutput.refPressure / spData.Count;
                avgPointOutput.refAirTemp = avgPointOutput.refAirTemp / spData.Count;

                return avgPointOutput;
            }

            return null;
        }

        

        public List<rawPDataPoint> avgPressureSPData(List<List<rawPDataPoint>> allData)
        {
            List<rawPDataPoint> compiledData = new List<rawPDataPoint>();





            return compiledData;
        }

        #endregion  //Ending processing region.

        public double calculateStDev(double[] numberList)
        {
            double arraySum = 0;
            foreach (double number in numberList)
            {
                arraySum += number;
            }
            double mean = arraySum / numberList.Length;
            double sumOfDevSquared = 0;
            foreach (double number in numberList)
            {
                sumOfDevSquared += Math.Pow((number - mean), 2);
            }
            return Math.Sqrt(sumOfDevSquared / (numberList.Length - 1));
        }

        public double calculateMean(double[] numberList)
        {
            double arraySum = 0;

            foreach (double number in numberList)
            {
                arraySum += number;
            }

            return arraySum / numberList.Length;
        }
    }

    [Serializable]
    class rawPDataPoint
    {
        //Radiosonde ID
        public string sondeID;

        //Radiosonde Pressure Data
        public double timeStamp;
        public Int32 pressureCount;
        public Int32 pressureCountRef;
        public Int32 ptCount;
        public Int32 ptCountRef;

        //Referance Sensor Data.
        public double refPressure;
        public double refAirTemp;
    }

    class pressureData
    {
        public double time;     //Time data collected.
        public Int32 pressureCount;     //Pressure Count.
        public Int32 pressureCountRef;  //Ramp Reference Count.
        public Int32 ptCount;           //Pressure Temperature Count.
        public Int32 ptCountRef;        //Pressure Temperearure Ramp Reference
    }

    class refData
    {
        public double time { get; set; }        //Reference Pressure Time.
        public double pressure { get; set; }    //Reference Pressure
        public double temperature { get; set; } //Reference Air Temperature
    }

    class alignedData
    {
        public refData pRef;
        public pressureData sData;
    }

    class spData
    {
        public List<alignedData> rawSPData = new List<alignedData>();
        public double avgRefPressure { get; set; }      //Average Pressure Reference
        public double avgRefAirTemp { get; set; }       //Average Air Temp Reference

        public double avgSondePCount { get; set; }      //Average Sonde Pressure Count
        public double avgSondePRCount { get; set; }     //Average Sonde Pressure Reference Count
        public double avgSondeTCount { get; set; }      //Average Sonde Temp Count
        public double avgSondeTRCount { get; set; }     //Average Sonde Temp Ref Count
        public double avgSondePressure { get; set; }    //Average Sonde Pressure
        public double avgSondeAirTemp { get; set; }     //Average Sonde Air Temp

        public double stDevRefPressure { get; set; }    //StDev Reference Pressure
        public double stDevRefAirTemp { get; set; }     //StDev Reference Air Temperature

        public double stDevSondePCount { get; set; }
        public double stDevSondePRCount { get; set; }     //StDev Sonde Pressure Reference Count
        public double stDevSondeTCount { get; set; }      //StDev Sonde Temp Count
        public double stDevSondeTRCount { get; set; }     //StDev Sonde Temp Ref Count
        public double stDevSondePressure { get; set; }    //StDev Sonde Pressure
        public double stDevSondeAirTemp { get; set; }       //StDev Sonde Ait Temperature
    }

    class runData
    {
        List<spData> SetPoints = new List<spData>();
        public double[] coeffiecients { get; set; }

    }
}

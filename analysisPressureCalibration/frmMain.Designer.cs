﻿namespace analysisPressureCalibration
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadValidateFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(945, 24);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadGroupsToolStripMenuItem,
            this.saveAnalysisToolStripMenuItem,
            this.loadValidateFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadGroupsToolStripMenuItem
            // 
            this.loadGroupsToolStripMenuItem.Name = "loadGroupsToolStripMenuItem";
            this.loadGroupsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.loadGroupsToolStripMenuItem.Text = "Load Group(s)";
            this.loadGroupsToolStripMenuItem.Click += new System.EventHandler(this.loadGroupsToolStripMenuItem_Click);
            // 
            // saveAnalysisToolStripMenuItem
            // 
            this.saveAnalysisToolStripMenuItem.Name = "saveAnalysisToolStripMenuItem";
            this.saveAnalysisToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.saveAnalysisToolStripMenuItem.Text = "Save Analysis";
            this.saveAnalysisToolStripMenuItem.Click += new System.EventHandler(this.saveAnalysisToolStripMenuItem_Click);
            // 
            // loadValidateFileToolStripMenuItem
            // 
            this.loadValidateFileToolStripMenuItem.Name = "loadValidateFileToolStripMenuItem";
            this.loadValidateFileToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.loadValidateFileToolStripMenuItem.Text = "Load Validate File";
            this.loadValidateFileToolStripMenuItem.Click += new System.EventHandler(this.loadValidateFileToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 543);
            this.Controls.Add(this.menuStripMain);
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "frmMain";
            this.Text = "Form1";
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadGroupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadValidateFileToolStripMenuItem;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace analysisPressureCalibration
{
    public partial class frmMain : Form
    {
        List<groupFiles> curPCalFiles = new List<groupFiles>();
        List<List<rawPDataPoint>> curAvgSondeData = new List<List<rawPDataPoint>>();    //Sonde and setpoint data.

        public frmMain()
        {
            InitializeComponent();
        }

        //Loading files.
        private void loadGroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog groupDia = new OpenFileDialog();
            groupDia.DefaultExt = "glog";
            groupDia.Multiselect = true;
            groupDia.Filter = "Group Files (.glog)|*.glog";

            string[] logFiles = null;

            if (DialogResult.OK == groupDia.ShowDialog())
            {
                logFiles = groupDia.FileNames;

                //Creating a compile list of the logged data and the referance file that is related to that data.
                foreach (string data in logFiles)
                {
                    //Breaking down the current location of the log file to get the location.
                    string[] dataBreakDown = data.Split('\\');

                    groupFiles tempGroup = new groupFiles();
                    
                    string[] fileData = System.IO.File.ReadAllLines(data);

                    for (int i = 0; i < fileData.Length - 1; i++)
                    {
                        string[] lineBreak = fileData[i].Split('\\');
                        tempGroup.sondeFiles.Add(lineBreak[lineBreak.Length-1]);
                    }

                    string[] refLineBreak = fileData[fileData.Length - 1].Split('\\');
                    tempGroup.refFile = refLineBreak[refLineBreak.Length - 1];

                    //finding the folder that the files are in.
                    string[] filenameBreak = data.Split('\\');
                    tempGroup.groupDir = data.Remove(data.Length - filenameBreak[filenameBreak.Length - 1].Length, filenameBreak[filenameBreak.Length-1].Length );

                    curPCalFiles.Add(tempGroup);

                }

                

                for(int i = 0; i< curPCalFiles.Count; i++)
                {

                }

                



            }
        }

        private void saveAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curAvgSondeData.Count > 0)
            {

                string totalData = "SondeID,";

                for (int x = 0; x < 32; x++)
                {
                    totalData += Convert.ToInt32(curAvgSondeData[0][x].refPressure).ToString() + ",";
                }

                totalData += "\n";

                foreach (List<rawPDataPoint> sonde in curAvgSondeData)
                {

                    totalData += sonde[0].sondeID + ",";

                    for (int i = 0; i < sonde.Count; i++)
                    {
                        totalData += sonde[i].pressureCount.ToString() + ",";
                    }

                    totalData += "\n";
                }

                System.IO.File.WriteAllText("output.csv", totalData);

            }
            else
            {
                MessageBox.Show("No Sonde Data Loaded.");
            }
        }

        private void loadValidateFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Doesn't work. Needs support class from calibration,,, however the data, and hardware items are all in the same class. This is causing hardware troubles.

            OpenFileDialog validateDataDialog = new OpenFileDialog();
            validateDataDialog.Title = "Load Bin";
            validateDataDialog.Filter = "Bin|*.bin";

            if (DialogResult.OK == validateDataDialog.ShowDialog())
            {
                List<object> localList;

                System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                System.IO.Stream stream = new System.IO.FileStream(validateDataDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                localList = (List<object>)formatter.Deserialize(stream);
                stream.Close();



            }
        }
    }

    public class groupFiles
    {
        public List<string> sondeFiles = new List<string>();
        public string refFile;
        public string groupDir;
    }
}

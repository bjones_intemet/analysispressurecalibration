﻿///Pressure Calibration Review requested by Joe Barnes
///2017/04/17

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pressureCalReview
{
    class Program
    {
        static List<PCalRun> CalibrationRuns = new List<PCalRun>();

        static void Main(string[] args)
        {

            string calDir = @"\\WMCHAMBER\PressureCalibrationData";

            //Pulling in all cal file dir info and sorting by name.
            var sortedDir = System.IO.Directory.GetDirectories(calDir).Select(fn => new System.IO.DirectoryInfo(fn)).OrderByDescending(f => f.Name).Take(200);

            //Single for testing.
            //var sortedDir = System.IO.Directory.GetDirectories(calDir, "20170330-1956").Select(fn => new System.IO.DirectoryInfo(fn)).OrderByDescending(f => f.Name);

            foreach (System.IO.DirectoryInfo calDirInfo in sortedDir)   //Going through the calibration files and getting the Raw and coef files.
            {
                CalibrationRuns.Add(new PCalRun         
                {
                    rawPCalFiles = System.IO.Directory.GetFiles(calDirInfo.FullName, "*.p_raw").Select(fi => new System.IO.FileInfo(fi)).OrderBy(f => f.Name),
                    coefFiles = System.IO.Directory.GetFiles(calDirInfo.FullName, "*.p_cal").Select(fi => new System.IO.FileInfo(fi)).OrderBy(f => f.Name),
                    groupFiles = System.IO.Directory.GetFiles(calDirInfo.FullName, "*.glog").Select(fi => new System.IO.FileInfo(fi)).OrderBy(f => f.Name),
                    refFiles = System.IO.Directory.GetFiles(calDirInfo.FullName, "*.p_ref").Select(fi => new System.IO.FileInfo(fi)).OrderBy(f => f.Name),
                    logFile = System.IO.Directory.GetFiles(calDirInfo.FullName, "debug.log").Select(fi => new System.IO.FileInfo(fi)).OrderBy(f => f.Name),
                });

                Console.WriteLine("Loaded " + calDirInfo.FullName);
            }

            Console.WriteLine("Hit enter to start");
            Console.ReadLine();

            int runsCounter = 0;

            foreach(PCalRun run in CalibrationRuns)
            {
                if (run.rawPCalFiles.Count() > 0 && run.refFiles.Count() > 0 && run.groupFiles.Count() > 0)
                {

                    Console.WriteLine("Getting the setpoints from log." + run.logFile.ToList()[0].Directory.ToString());
                    run.logCalPoints = processLogFile(run.logFile.ToList()[0].FullName);

                    Console.WriteLine("Finding Sync Time");
                    run.syncTime = findTimeSync(run.logFile.ToList()[0].FullName, run.groupFiles.ToList()[0].FullName);

                    Console.WriteLine("Loading Reference Data and Time Syncing");
                    run.Reference = loadReferenceData(run.refFiles, run.syncTime);

                    Console.WriteLine("Loading Radiosonde Data.");
                    run.Radiosondes = loadRadiosondes(run.rawPCalFiles, run.coefFiles, run.syncTime);

                    Console.WriteLine("Compileing all data together.");
                    run.logCalPoints = findSPData(run.logCalPoints, run.Radiosondes, run.Reference);

                    Console.WriteLine("Building results.");
                    run.results = getResults(run);
                    //generateReport(run);

                    Console.WriteLine("Completed " + runsCounter.ToString() + " of " + CalibrationRuns.Count().ToString());
                    Console.WriteLine();
                }

                runsCounter++;
            }

            //Final system report
            List<string> reportData = new List<string>();
            reportData.Add("RunName,StartTime,ElapseTime,RCount,BadCount,Precent,Leaks,FailedRadiosondes,VRunFail");

            foreach(PCalRun run in CalibrationRuns)
            {
                if (run.results != null)
                {
                    string leakInfo = "None";
                    if (run.results.Leaks.Count > 0)
                    {
                        leakInfo = "";

                        for (int i = 0; i < run.results.Leaks.Count(); i++)
                        {
                            leakInfo += run.results.Leaks[i].Notes.Replace(',', ' ') + "|";
                        }
                    }

                    string failed = "";
                    for(int i = 0; i< run.results.FailedRadiosondes.Count(); i++)
                    {
                        failed += run.results.FailedRadiosondes[i] + "|";
                    }

                    reportData.Add(run.results.RunName + "," + run.results.TimeStarted.ToString("yyyy/MM/dd HH:mm:ss") + "," +
                        run.results.ElapsedTime.Hours.ToString("00") + ":" + run.results.ElapsedTime.Minutes.ToString("00") + ":" + run.results.ElapsedTime.Seconds.ToString("00") + "," +
                        run.Radiosondes.Count().ToString() + "," + run.results.badCount.ToString() + "," + (((double)run.results.badCount / (double)run.Radiosondes.Count()) * 100).ToString("0.00") + "%," +
                        leakInfo + "," + failed);
                }
            }

            Console.WriteLine("Writing report file.");
            System.IO.File.WriteAllLines("Runs.csv", reportData.ToArray());


            //finding the conditons where radiosondes fail.
            List<PointOfFailure> totalPointsOfFailure = new List<PointOfFailure>();
            for (int i = 0; i < CalibrationRuns.Count(); i++)
            {
                totalPointsOfFailure.AddRange(evalSP(CalibrationRuns[i]));
            }

            List<double> ConditionAT = new List<double>();
            List<double> ConditionPressure = new List<double>();

            string[] calConfigLines = System.IO.File.ReadAllLines( AppDomain.CurrentDomain.BaseDirectory + "calibrationHighToLow.cfg");
            for(int i = 0; i< calConfigLines.Length; i++)
            {
                if (!calConfigLines[i].Contains("#"))
                {
                    string[] breakLine = calConfigLines[i].Split(',');

                    if (calConfigLines[i].Contains("TSP"))
                    {
                        ConditionAT.Add(Convert.ToDouble(breakLine[2]));
                    }
                    if (calConfigLines[i].Contains("PSP"))
                    {
                        ConditionPressure.Add(Convert.ToDouble(breakLine[2]));
                    }
                }
            }

            List<CalibrationPoint> CalPoints = new List<CalibrationPoint>();
            for(int i = 0; i<ConditionAT.Count; i++)
            {
                for (int p = 0; p < ConditionPressure.Count; p++)
                {
                    CalPoints.Add(new CalibrationPoint
                    {
                        SetPointAT = ConditionAT[i],
                        SetPointPressure = ConditionPressure[p],
                        Failures = totalPointsOfFailure.Where(conditon => (conditon.atSetPoint == ConditionAT[i]) && (conditon.pSetPoint == ConditionPressure[p])).ToList()
                    });
                }
            }
            Console.WriteLine("Compiling the Failures at calibration points.");

            List<string> calPointsReport = new List<string>();
            calPointsReport.Add("Temp,Pressure,Count");
            for (int i = 0; i < CalPoints.Count(); i++)
            {
                calPointsReport.Add(CalPoints[i].SetPointAT.ToString("0.0") + "," + CalPoints[i].SetPointPressure.ToString("0.0") + "," + CalPoints[i].Failures.Count().ToString());
            }

            System.IO.File.WriteAllLines("SPReport.csv", calPointsReport.ToArray());

            Console.WriteLine("Complete!");
            Console.ReadLine();
        }

        static List<CalLogPoints> processLogFile(string fileName)
        {
            List<CalLogPoints> allCalPoints = new List<CalLogPoints>();

            string[] logDataLines = System.IO.File.ReadAllLines(fileName);

            //Getting the run date.
            string[] breakFileName = fileName.Split(new char[] { '\\', '-' });
            int year = Convert.ToInt16(breakFileName[breakFileName.Length - 3].Substring(0, 4));
            int month = Convert.ToInt16(breakFileName[breakFileName.Length - 3].Substring(4, 2));
            int day = Convert.ToInt16(breakFileName[breakFileName.Length - 3].Substring(6, 2));

            //Main Containers.
            DateTime currentSetPointTimeStart = new DateTime(1900, 1, 1);
            DateTime currentSetPointTimeEnd = new DateTime(1900, 1, 1);
            DateTime currentLeakTime = new DateTime(1900, 1, 1);
            double currentAirTempSP = -9999;
            double currentPressure = -9999;
            bool curretLeak = false;
            string leakID = "";
            



            for(int i = 0; i< logDataLines.Length; i++)     //Going through the log lines.
            {
                if(logDataLines[i].Contains("Calibration Complete. Starting Post Process."))
                {
                    break;
                }

                if(logDataLines[i].Contains("Chamber set to:"))    //Found the start of a new calibration point.
                {
                    string[] breakChamberSetPoint = logDataLines[i].Split(new char[] {':', ' ', '.' }, StringSplitOptions.RemoveEmptyEntries);
                    currentAirTempSP = Convert.ToDouble(breakChamberSetPoint[breakChamberSetPoint.Length - 1]);

                    currentSetPointTimeStart = new DateTime(year, month, day, Convert.ToInt16(breakChamberSetPoint[0]), Convert.ToInt16(breakChamberSetPoint[1]),
                    Convert.ToInt16(breakChamberSetPoint[2]), Convert.ToInt16(breakChamberSetPoint[3])/10);
                }

                if (logDataLines[i].Contains("Leak found."))
                {
                    string[] breakStartTime = logDataLines[i].Split(new char[] { ':', '.', ' ', '-' }, StringSplitOptions.RemoveEmptyEntries);
                    currentLeakTime = new DateTime(year, month, day, Convert.ToInt16(breakStartTime[0]), Convert.ToInt16(breakStartTime[1]),
                    Convert.ToInt16(breakStartTime[2]), Convert.ToInt16(breakStartTime[3]) / 10);

                    string port = "";
                    string rate = "";

                    for(int b = 0; b< 100; b++)
                    {
                        if(logDataLines[i-b].Contains("Leak status: True"))
                        {
                            string[] breakRateLine = logDataLines[i - b - 1].Split(' ');
                            rate = breakRateLine.Last();

                            string[] breakPortLine = logDataLines[i - b - 16].Split(' ');
                            port = breakPortLine.Last();
                        }
                    }

                    if(port == "")
                    {
                        rate = "Unable to stabilize";

                        string[] breakBadLeak = logDataLines[i-2].Split(' ');
                        port = breakBadLeak.Last();
                    }


                    leakID = port + "," + rate;

                    curretLeak = true;
                }


                //Opening goup 0 manifold ports.
                if (logDataLines[i].Contains("Opening goup 0 manifold ports."))
                {
                    string[] breakLine = logDataLines[i - 1].Split(' ', '-', ',');
                    currentPressure = Convert.ToDouble(breakLine[6]);
                }

                if (logDataLines[i].Contains("Moving to next calibration step."))
                {
                    string[] breakStartTime = logDataLines[i].Split(new char[] { ':', '.', ' ', '-' }, StringSplitOptions.RemoveEmptyEntries);

                    currentSetPointTimeEnd = new DateTime(year, month, day, Convert.ToInt16(breakStartTime[0]), Convert.ToInt16(breakStartTime[1]),
                            Convert.ToInt16(breakStartTime[2]), Convert.ToInt16(breakStartTime[3]) / 10);

                    if (allCalPoints.Count > 0)    //Checking for clock roll over.
                    {
                        TimeSpan rollCheckStart = currentSetPointTimeStart - allCalPoints[0].SetPointTimeStart;
                        TimeSpan rollCheckEnd = currentSetPointTimeEnd - allCalPoints[0].SetPointTimeStart;

                        if(rollCheckStart.TotalSeconds < 0)
                        {
                            currentSetPointTimeStart = currentSetPointTimeStart.AddDays(1);
                        }

                        if (rollCheckEnd.TotalSeconds < 0)
                        {
                            day++;
                            try
                            {
                                DateTime testDate = new DateTime(year, month, day);
                            }
                            catch
                            {
                                month++;
                                day = 1;

                                try
                                {
                                    DateTime testDate = new DateTime(year, month, day);
                                }
                                catch
                                {
                                    year++;
                                    month = 1;
                                    day = 1;
                                }

                            }
                            currentSetPointTimeEnd = new DateTime(year, month, day, Convert.ToInt16(breakStartTime[0]), Convert.ToInt16(breakStartTime[1]),
                            Convert.ToInt16(breakStartTime[2]), Convert.ToInt16(breakStartTime[3]) / 10);

                        }
                    }

                    allCalPoints.Add(new CalLogPoints
                    {
                        SetPointTimeStart = currentSetPointTimeStart,
                        SetPointTimeEnd = currentSetPointTimeEnd,
                        LeakTime = currentLeakTime,
                        pressure = currentPressure,
                        airTemp = currentAirTempSP,
                        LeakDetected = curretLeak,
                        leakingRunner = leakID

                    });

                    leakID = "";
                    curretLeak = false;
                    currentLeakTime = new DateTime(1900, 1, 1);

                    string[] breakChamberSetPoint = logDataLines[i].Split(new char[] { ':', ' ', '.' }, StringSplitOptions.RemoveEmptyEntries);

                    currentSetPointTimeStart = new DateTime(year, month, day, Convert.ToInt16(breakChamberSetPoint[0]), Convert.ToInt16(breakChamberSetPoint[1]),
                    Convert.ToInt16(breakChamberSetPoint[2]), Convert.ToInt16(breakChamberSetPoint[3]) / 10);

                }
            }

            return allCalPoints;
        }

        /// <summary>
        /// Finds the sync time from system time to elapse time.
        /// </summary>
        /// <param name="logFileName"></param>
        /// <param name="zeroGroupFileName"></param>
        /// <returns></returns>
        static SynceDateTime findTimeSync(string logFileName, string zeroGroupFileName)
        {
            string[] logLines = System.IO.File.ReadAllLines(logFileName);

            //Getting the run date.
            string[] breakFileName = logFileName.Split(new char[] { '\\', '-' }, StringSplitOptions.RemoveEmptyEntries);
            int year = Convert.ToInt16(breakFileName[breakFileName.Length - 3].Substring(0, 4));
            int month = Convert.ToInt16(breakFileName[breakFileName.Length - 3].Substring(4, 2));
            int day = Convert.ToInt16(breakFileName[breakFileName.Length - 3].Substring(6, 2));

            DateTime currentSyncTime = new DateTime(1900, 01, 01);

            for (int i = 0; i < logLines.Length; i++)
            {
                if(logLines[i].Contains("Logging: 0"))
                {
                    string[] breakCurrentLine = logLines[i].Split(new char[] { ':', ' ', '.' }, StringSplitOptions.RemoveEmptyEntries);

                    currentSyncTime = new DateTime(year, month, day, Convert.ToInt16(breakCurrentLine[0]), Convert.ToInt16(breakCurrentLine[1]),
                    Convert.ToInt16(breakCurrentLine[2]), Convert.ToInt16(breakCurrentLine[3]) / 10);

                    break;
                }
            }

            string[] zeroGroupLines = System.IO.File.ReadAllLines(zeroGroupFileName);

            string[] firstFile = zeroGroupLines[0].Split('\\');

            if(firstFile.Last() == "_.p_raw")
            {
                firstFile = zeroGroupLines[1].Split('\\');
            }

            string fileToLoad = logFileName.Replace(breakFileName[breakFileName.Length - 1], firstFile.Last());

            string[] rawDataLines = System.IO.File.ReadAllLines(fileToLoad);
    
            string[] breakFirstDataLogged = rawDataLines[5].Split(',');

            return new SynceDateTime
            {
                syncSystemTime = currentSyncTime,
                synceElapseTime = Convert.ToDouble(breakFirstDataLogged[0])
            };

        }

        static List<RadiosondeData> loadRadiosondes (System.Linq.IOrderedEnumerable<System.IO.FileInfo> rawPCalFiles, System.Linq.IOrderedEnumerable<System.IO.FileInfo> coefFiles, SynceDateTime curSync)
        {
            List<RadiosondeData> sondes = new List<RadiosondeData>();

            foreach (System.IO.FileInfo rf in rawPCalFiles)
            {
                RadiosondeData tempRadiosonde = new RadiosondeData();
                tempRadiosonde.SerialNumber = rf.Name.Replace(rf.Extension, "").Replace("_", "");

                try
                {
                    System.IO.FileInfo curCoef = coefFiles.FirstOrDefault(e => e.FullName.Contains(rf.Name.Replace(rf.Extension, "").Replace("_", "")));
                    tempRadiosonde.Coefficients = loadPCoef(curCoef.FullName);
                }
                catch
                {
                    tempRadiosonde.passFail = false;
                    tempRadiosonde.Coefficients = new double[]     //Defaul coef that should make the readings look like crap.
                    {
                        -1e+02, -1.65889122e+00, 4.17678815e-04, 1.51478315e-06, 0.00000000e+00, 2.45368631e+03, 5.12847742e+00, -8.67345619e-04,
                        0.00000000e+00, 0.00000000e+00,-2.84206870e+01,-1.57200413e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,4.20982924e+01,0.00000000e+00,
                        0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,+6.04000000e+03,
                        +5.00000000e+03,+3.37500000e+03,+2.98150000e+02
                    };
                }

                tempRadiosonde.runData = loadRadiosondeData(rf.FullName, tempRadiosonde.Coefficients, curSync);

                sondes.Add(tempRadiosonde);
            }

            return sondes;
        }

        static double[] loadPCoef(string fileName)
        {
            List<double> coefs = new List<double>();

            string[] coefsLines = System.IO.File.ReadAllLines(fileName);

            for(int i = 1; i< coefsLines.Length; i++)
            {
                coefs.Add(Convert.ToDouble(coefsLines[i]));
            }

            return coefs.ToArray();
        }

        static List<RadiosondeDataLine> loadRadiosondeData(string fileName, double[] coef, SynceDateTime curSync)
        {
            List<RadiosondeDataLine> data = new List<RadiosondeDataLine>();

            Pressure_Calculator.Pressure_Calculator processor = new Pressure_Calculator.Pressure_Calculator();

            string[] sondeLines = System.IO.File.ReadAllLines(fileName);

            for (int i = 5; i < sondeLines.Length; i++)
            {
                string[] breakLine = sondeLines[i].Split(',');

                data.Add(new RadiosondeDataLine
                {
                    ElapseTime = Convert.ToDouble(breakLine[0]),
                    PressureCount = Convert.ToDouble(breakLine[1]),
                    PressureCountRef = Convert.ToDouble(breakLine[2]),
                    TempCount = Convert.ToDouble(breakLine[3]),
                    TempCountRef = Convert.ToDouble(breakLine[4]),
                    CalcAirTemp = processor.calcPressureTemp(Convert.ToDouble(breakLine[3]), Convert.ToDouble(breakLine[4]))
                });

                data.Last().CalcPressure = processor.calcPressure(data.Last().PressureCount, data.Last().PressureCountRef, data.Last().CalcAirTemp, coef);
                data.Last().SystemTime = getSystemTime(data.Last().ElapseTime, curSync);
            }

            return data;
        }

        static List<ReferenceDataLine> loadReferenceData(System.Linq.IOrderedEnumerable<System.IO.FileInfo> refFiles, SynceDateTime curSync)
        {
            List<ReferenceDataLine> refData = new List<ReferenceDataLine>();

            foreach(System.IO.FileInfo fi in refFiles)
            { 
                string[] refLines = System.IO.File.ReadAllLines(fi.FullName);

                for (int i = 5; i < refLines.Length; i++)
                {
                    string[] lineBreak = refLines[i].Split(',');

                    refData.Add(new ReferenceDataLine
                    {
                        ElapseTime = Convert.ToDouble(lineBreak[0]),
                        PressureReference = Convert.ToDouble(lineBreak[1]),
                        ReferenceAirTemp = Convert.ToDouble(lineBreak[2])
                    });

                    refData.Last().SystemTime = getSystemTime(refData.Last().ElapseTime, curSync);

                }

            }

            //Sorting for elapsed time.
            var sorted = refData.OrderBy(e => e.ElapseTime);
            return refData = (List<ReferenceDataLine>)sorted.ToList();
        }

        static DateTime getSystemTime(double elapse, SynceDateTime curSync)
        {
            double timeFromSync = elapse - curSync.synceElapseTime;
            return curSync.syncSystemTime.AddSeconds(timeFromSync);
        }

        /// <summary>
        /// Finding the data related to each sp.
        /// </summary>
        /// <param name=""></param>
        /// <param name="Radiosondes"></param>
        /// <param name="Reference"></param>
        static List<CalLogPoints> findSPData(List<CalLogPoints> logPoints, List<RadiosondeData> Radiosondes, List<ReferenceDataLine> Reference)
        {
            foreach(CalLogPoints sp in logPoints)
            {
                sp.RadiosondeSPData = new List<RadiosondeData>();
                sp.DroppedError = new List<string>();

                for (int i = 0; i< Radiosondes.Count(); i++)
                {
                    sp.RadiosondeSPData.Add(new RadiosondeData
                    {
                        SerialNumber = Radiosondes[i].SerialNumber,
                        Coefficients = Radiosondes[i].Coefficients,
                        runData = Radiosondes[i].runData
                    });
                }
                
                foreach(RadiosondeData rd in sp.RadiosondeSPData)
                {
                    var sondeTimeData = rd.runData.Where(p => p.SystemTime >= sp.SetPointTimeStart && p.SystemTime <= sp.SetPointTimeEnd);
                    rd.runData = sondeTimeData.ToList();
                }

                var refTimeData = Reference.Where(p => p.SystemTime >= sp.SetPointTimeStart && p.SystemTime <= sp.SetPointTimeEnd);
                sp.ReferenceSPData = refTimeData.ToList();

                foreach (RadiosondeData rd in sp.RadiosondeSPData)  //Matching the sonde to ref and then diffing.
                {
                    if (rd.runData.Count() > 0)
                    {
                        foreach (RadiosondeDataLine rdl in rd.runData)
                        {
                            var match = sp.ReferenceSPData.Where(m => m.SystemTime >= rdl.SystemTime.AddSeconds(-0.25) && m.SystemTime <= rdl.SystemTime.AddSeconds(0.25)).ToList();
                            rdl.DiffPressure = rdl.CalcPressure - match[0].PressureReference;
                            rdl.DiffAirTemp = rdl.CalcAirTemp - match[0].ReferenceAirTemp;

                            rd.avgPressure += rdl.CalcPressure;
                            rd.avgPressureAT += rdl.CalcAirTemp;
                            rd.avgDiffPressure += rdl.DiffPressure;
                            rd.avgDiffPressureAT += rdl.DiffAirTemp;
                        }

                        rd.avgPressure = rd.avgPressure / rd.runData.Count();
                        rd.avgPressureAT = rd.avgPressureAT / rd.runData.Count();
                        rd.avgDiffPressure = rd.avgDiffPressure / rd.runData.Count();
                        rd.avgDiffPressureAT = rd.avgDiffPressureAT / rd.runData.Count();
                    }
                    else
                    {

                        rd.avgDiffPressure = -9999;
                        rd.avgDiffPressureAT = -9999;
                        rd.avgPressure = -9999;
                        rd.avgPressureAT = -9999;
                        rd.passFail = false;
                        
                    }
                }

            }

            return logPoints;
        }

        static void generateReport(PCalRun run)
        {
            string header = "";
            header += "Start Time," + run.logCalPoints[0].SetPointTimeStart.ToString("yyyy/MM/dd HH:mm:ss.fff") + "\r\n";
            header += "End Time," + run.logCalPoints.Last().SetPointTimeEnd.ToString("yyyy/MM/dd HH:mm:ss.fff") + "\r\n";
            TimeSpan calTime = (run.logCalPoints.Last().SetPointTimeEnd - run.logCalPoints[0].SetPointTimeStart);
            header += "Elapsed Time," + calTime.Hours.ToString("00") + ":" + calTime.Minutes.ToString("00") + ":" + calTime.Seconds.ToString("00") + "\r\n";

            header += "SN,Pass/Fail,";

            foreach(CalLogPoints lp in run.logCalPoints)
            {
                header += lp.airTemp.ToString("0") + "@" + lp.pressure.ToString("0") + ",";
            }

            header += "\r\n";

            string[] radiosondeLines = new string[run.logCalPoints[0].RadiosondeSPData.Count];
            bool[] passFail = new bool[run.logCalPoints[0].RadiosondeSPData.Count];

            for (int i = 0; i < passFail.Length; i++)
            {
                passFail[i] = true;
            }

            for(int i = 0; i< run.logCalPoints.Count; i++)
            {
                for(int x = 0; x< run.logCalPoints[i].RadiosondeSPData.Count; x++)
                {
                    if (passFail[x])
                    {
                        
                        if (run.logCalPoints[i].RadiosondeSPData[x].avgDiffPressure > 0.4 ||
                            run.logCalPoints[i].RadiosondeSPData[x].avgDiffPressure < -0.4 ||
                            run.logCalPoints[i].RadiosondeSPData[x].runData.Count == 0)
                        {
                            passFail[x] = false;
                        }
                        else
                        {
                            passFail[x] = true;
                        }
                        
                    }

                    radiosondeLines[x] = run.logCalPoints[i].RadiosondeSPData[x].SerialNumber + ",";
                }
            }

            for (int i = 0; i < passFail.Length; i++)
            {
                radiosondeLines[i] += passFail[i].ToString() + ",";
            }

            foreach (CalLogPoints lp in run.logCalPoints)
            {
                for (int i = 0; i < lp.RadiosondeSPData.Count; i++)
                {
                    radiosondeLines[i] += lp.RadiosondeSPData[i].avgDiffPressure.ToString("0.000") + ",";
                }
            }

            string totalOutput = header;
            for(int i = 0; i< radiosondeLines.Length; i++)
            {
                totalOutput += radiosondeLines[i] + "\r\n";
            }

            string[] breakRunName = run.rawPCalFiles.ToList()[0].FullName.Split('\\');

            System.IO.File.WriteAllText(breakRunName[breakRunName.Length - 2] + "-Results.csv", totalOutput);
        }

        static ReportData getResults(PCalRun run)
        {
            ReportData results = new ReportData();

            string[] breakRunName = run.rawPCalFiles.ToList()[0].FullName.Split('\\');
            results.RunName = breakRunName[breakRunName.Length - 2];

            //Finding if the radiosonde fails any set point.
            for (int i = 0; i < run.logCalPoints.Count; i++)
            {
                for (int x = 0; x < run.logCalPoints[i].RadiosondeSPData.Count; x++)
                {
                    if (run.logCalPoints[i].RadiosondeSPData[x].avgDiffPressure > 0.4 ||
                        run.logCalPoints[i].RadiosondeSPData[x].avgDiffPressure < -0.4 ||
                        run.logCalPoints[i].RadiosondeSPData[x].runData.Count == 0)
                    {
                        run.logCalPoints[i].RadiosondeSPData[x].passFail = false;
                    }
                    else
                    {
                        run.logCalPoints[i].RadiosondeSPData[x].passFail = true;
                    }
                }
            }

            //Updating the radiosonde list.
            foreach(CalLogPoints lp in run.logCalPoints)
            {
                foreach(RadiosondeData rd in lp.RadiosondeSPData)
                {
                    var sonde = run.Radiosondes.Where(s => s.SerialNumber == rd.SerialNumber).ToList();

                    if(sonde[0].passFail == true)
                    {
                        sonde[0].passFail = rd.passFail;
                    }

                    if(rd.passFail == false)
                    {
                        System.Diagnostics.Debug.WriteLine(rd.SerialNumber + "," + rd.avgPressure.ToString("0.00") + "," + rd.avgPressureAT.ToString("0.00"));
                    }
                    
                }

                if(lp.LeakDetected)
                {
                    results.Leaks.Add(new LeakData
                    {
                        LeakTime = lp.LeakTime,
                        Notes = lp.leakingRunner
                    });
                }


            }

            foreach(RadiosondeData rd in run.Radiosondes)
            {
                if(!rd.passFail)
                {
                    results.FailedRadiosondes.Add(rd.SerialNumber);
                }
            }

            results.badCount = run.Radiosondes.Count(n => n.passFail == false);
            results.TimeStarted = run.logCalPoints.First().SetPointTimeStart;
            results.TimeEnded = run.logCalPoints.Last().SetPointTimeEnd;
            results.ElapsedTime = results.TimeEnded - results.TimeStarted;

            return results;
        }

        static List<PointOfFailure> evalSP(PCalRun run)
        {
            List<PointOfFailure> results = new List<PointOfFailure>();

            if (run.logCalPoints != null)
            {
                foreach (CalLogPoints lp in run.logCalPoints)
                {
                    foreach (RadiosondeData rd in lp.RadiosondeSPData)
                    {
                        if (!rd.passFail && rd.runData.Count() > 0 && rd.Coefficients[0] != -1e+02)
                        {
                            results.Add(new PointOfFailure
                            {
                                pSetPoint = lp.pressure,
                                atSetPoint = lp.airTemp,
                                RefData = lp.ReferenceSPData,
                                failedRadiosonde = rd
                            });
                        }
                    }
                }
            }

            return results;

        }
    }

    class PCalRun
    {
        public System.Linq.IOrderedEnumerable<System.IO.FileInfo> rawPCalFiles { get; set; }
        public System.Linq.IOrderedEnumerable<System.IO.FileInfo> coefFiles { get; set; }
        public System.Linq.IOrderedEnumerable<System.IO.FileInfo> groupFiles { get; set; }
        public System.Linq.IOrderedEnumerable<System.IO.FileInfo> refFiles { get; set; }
        public System.Linq.IOrderedEnumerable<System.IO.FileInfo> logFile { get; set; }

        public List<CalLogPoints> logCalPoints { get; set; }
        public SynceDateTime syncTime { get; set; }

        public List<RadiosondeData> Radiosondes = new List<RadiosondeData>();
        public List<ReferenceDataLine> Reference = new List<ReferenceDataLine>();

        public ReportData results { get; set; }

    }

    class CalLogPoints
    {
        public DateTime SetPointTimeStart { get; set; }
        public DateTime SetPointTimeEnd { get; set; }
        public DateTime LeakTime { get; set; }
        public double airTemp { get; set; }
        public double pressure { get; set; }
        public bool LeakDetected { get; set; }  //Was a leak detected.
        public string leakingRunner { get; set; }   //Runner that leaked.

        public List<RadiosondeData> RadiosondeSPData { get; set; }
        public List<ReferenceDataLine> ReferenceSPData { get; set; }
        public List<string> DroppedError { get; set; }
    }

    class SynceDateTime
    {
        public DateTime syncSystemTime { get; set; }
        public double synceElapseTime { get; set; }
    }

    class RadiosondeData
    {
        public string SerialNumber { get; set; }
        public double[] Coefficients { get; set; }
        public List<RadiosondeDataLine> runData { get; set; }
        public double avgPressure { get; set; }
        public double avgPressureAT { get; set; }
        public double avgDiffPressure { get; set; }
        public double avgDiffPressureAT { get; set; }
        public bool passFail { get; set; }

        public RadiosondeData()
        {
            passFail = true;
        }
    }

    class RadiosondeDataLine
    {
        public double ElapseTime { get; set; }
        public DateTime SystemTime { get; set; }
        public double PressureCount { get; set; }
        public double PressureCountRef { get; set; }
        public double TempCount { get; set; }
        public double TempCountRef { get; set; }
        public double CalcPressure { get; set; }
        public double CalcAirTemp { get; set; }
        public double DiffPressure { get; set; }
        public double DiffAirTemp { get; set; }
    }

    class ReferenceDataLine
    {
        public double ElapseTime { get; set; }
        public DateTime SystemTime { get; set; }
        public double PressureReference { get; set; }
        public double ReferenceAirTemp { get; set; }
    }

    class LeakData
    {
        public DateTime LeakTime { get; set; }
        public string Notes { get; set; }
    }

    class ReportData
    {
        public string RunName { get; set; }
        public int badCount { get; set; }
        public TimeSpan ElapsedTime { get; set; }
        public DateTime TimeStarted { get; set; }
        public DateTime TimeEnded { get; set; }
        public List<LeakData> Leaks = new List<LeakData>();
        public List<string> FailedRadiosondes = new List<string>();

    }

    class PointOfFailure
    {
        public double pSetPoint { get; set; }
        public double atSetPoint { get; set; }
        public RadiosondeData failedRadiosonde { get; set; }

        public List<ReferenceDataLine> RefData = new List<ReferenceDataLine>();
        
    }

    class CalibrationPoint
    {
        public double SetPointAT { get; set; }
        public double SetPointPressure { get; set; }

        public List<PointOfFailure> Failures { get; set; }
    }
}
